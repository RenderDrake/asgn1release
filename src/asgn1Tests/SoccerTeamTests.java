package asgn1Tests;

import static org.junit.Assert.*;


import org.junit.Test;

import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerTeam;




/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */
public class SoccerTeamTests {
	
	private final int homeGoals = 5;
	private final int awayGoals = 4;
	private final int wonMatch = 1;
	private final int homeCompare  = -3;
	private final int awayCompare = 3;
	/*
	 * Tests whether a SoccerTeam with default values can be created
	 */
	@Test
	public void SoccerTeamTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			assertEquals("-----", genericTeam.getFormString());
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the official name of a team returns correctly
	 */
	@Test
	public void getOfficialNameTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			assertEquals(genericTeam.getOfficialName(), "generic");
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
	/*
	 * Tests whether the nickname of a team returns correctly
	 */
	@Test
	public void getNickNameTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			assertEquals(genericTeam.getNickName(), "team");
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the goals scored returns correctly
	 */
	@Test
	public void getGoalsScoredTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(homeGoals, awayGoals);
			assertEquals(genericTeam.getGoalsScoredSeason(), homeGoals);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the goals conceded returns correctly
	 */
	@Test
	public void getGoalsConcededTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(homeGoals, awayGoals);
			assertEquals(genericTeam.getGoalsConcededSeason(), awayGoals);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether matches won returns correctly
	 */
	@Test
	public void getMatchesWonTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(homeGoals, awayGoals);
			assertEquals(genericTeam.getMatchesWon(), wonMatch);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether matches lost returns correctly
	 */
	@Test
	public void getMatchesLostTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(awayGoals, homeGoals);
			assertEquals(genericTeam.getMatchesLost(), wonMatch);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether matches drawn returns correctly
	 */
	@Test
	public void getMatchesDrawnTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(homeGoals, homeGoals);
			assertEquals(genericTeam.getMatchesDrawn(), wonMatch);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether competitions points returns correctly
	 */
	@Test
	public void getCompetitionPointsTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(homeGoals, homeGoals);
			assertEquals(genericTeam.getCompetitionPoints(), wonMatch);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests wehter goal difference returns correctly after a match
	 */
	@Test
	public void getGoalDifferenceTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(homeGoals, homeGoals);
			assertEquals(genericTeam.getGoalDifference(), homeGoals-homeGoals);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether form strings return correctly
	 */
	@Test
	public void getFormStringTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(homeGoals, homeGoals);
			assertEquals(genericTeam.getFormString(), "D----");
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether a match can be played
	 */
	@Test
	public void playMatchTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, homeGoals);
			assertEquals(genericTeam.getFormString(), "LDDDD");
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether compareTo returns correctly
	 */
	@Test
	public void compareToTester(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			SoccerTeam otherTeam = new SoccerTeam("other", "team");
			genericTeam.playMatch(homeGoals, awayGoals);
			otherTeam.playMatch(awayGoals, homeGoals);
			assertEquals(genericTeam.compareTo(otherTeam), homeCompare);
			assertEquals(otherTeam.compareTo(genericTeam), awayCompare);
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether stats of teams can be reset to default values
	 */
	@Test
	public void resetStats(){
		try {
			SoccerTeam genericTeam = new SoccerTeam("generic", "team");
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, awayGoals);
			genericTeam.playMatch(awayGoals, homeGoals);
			genericTeam.resetStats();
			assertEquals(genericTeam.getGoalsScoredSeason(), 0);
			assertEquals(genericTeam.getGoalsConcededSeason(), 0);
			assertEquals(genericTeam.getMatchesWon(), 0);
			assertEquals(genericTeam.getMatchesLost(), 0);
			assertEquals(genericTeam.getMatchesDrawn(), 0);
			assertEquals(genericTeam.getCompetitionPoints(), 0);
			assertEquals(genericTeam.getFormString(), "-----");
			
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}

package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;


/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */
public class SoccerLeagueTests {
		
	private final int capacity = 1;
	private final int matchCapacity = 2;
	private final int homeGoals = 5;
	private final int awayGoals = 6;
	/*
	 * Tests whether a SoccerLeague can be initialised
	 */
	@Test
	public void SoccerLeagueTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
	}
	/*
	 * Tests whether a SoccerLeague can register a SoccerTeam
	 */
	@Test
	public void registerTeamTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		try {
			testLeague.registerTeam(new SoccerTeam("generic", "team"));
		} catch (LeagueException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether a SoccerLeague can remove a SoccerTeam
	 */
	@Test
	public void removeTeamTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		try {
			testLeague.registerTeam(new SoccerTeam("generic", "team"));
			testLeague.removeTeam(testLeague.getTeamByOfficalName("generic"));
		} catch (LeagueException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the number of registered teams can be returned
	 */
	@Test
	public void getRegisteredNumTeamsTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		try {
			testLeague.registerTeam(new SoccerTeam("generic", "team"));
			assertEquals(testLeague.getRegisteredNumTeams(), capacity);
		} catch (LeagueException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether a SoccerLeague can be initialised
	 */
	@Test
	public void getRequiredNumTeamsTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		assertEquals(testLeague.getRequiredNumTeams(), capacity);
	}
	/*
	 * Tests whether the league can start a new season
	 */
	@Test
	public void startNewSeasonTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		try {
			testLeague.registerTeam(new SoccerTeam("generic", "team"));
			testLeague.startNewSeason();
		} catch (LeagueException | TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the league can end a season
	 */
	@Test
	public void endSeasonTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		try {
			testLeague.registerTeam(new SoccerTeam("generic", "team"));
			testLeague.startNewSeason();
			testLeague.endSeason();
		} catch (LeagueException | TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the season's status can be returned
	 */
	@Test
	public void isOffSeasonTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		assertEquals(testLeague.isOffSeason(), true);
	}
	/*
	 * Tests whether the getTeamByOfficialName function returns correctly
	 */
	@Test
	public void getTeamByOfficialNameTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		try {
			SoccerTeam testTeam = new SoccerTeam("generic", "team");
			testLeague.registerTeam(testTeam);
			assertEquals(testLeague.getTeamByOfficalName("generic"), testTeam);
		} catch (LeagueException | TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether a match can be played between two teams
	 */
	@Test
	public void playMatchTester(){
		SoccerLeague testLeague = new SoccerLeague(matchCapacity);
		try {
			SoccerTeam homeTeam = new SoccerTeam("home", "team");
			SoccerTeam awayTeam = new SoccerTeam("away", "team");
			testLeague.registerTeam(homeTeam);
			testLeague.registerTeam(awayTeam);
			testLeague.startNewSeason();
			testLeague.playMatch("home", homeGoals, "away", awayGoals);
		} catch (TeamException | LeagueException e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the top team of the league can be returned
	 */
	@Test
	public void getTopTeamTester(){
		SoccerLeague testLeague = new SoccerLeague(matchCapacity);
		try {
			SoccerTeam homeTeam = new SoccerTeam("home", "team");
			SoccerTeam awayTeam = new SoccerTeam("away", "team");
			testLeague.registerTeam(homeTeam);
			testLeague.registerTeam(awayTeam);
			testLeague.startNewSeason();
			testLeague.playMatch("home", homeGoals, "away", awayGoals);
			assertEquals(testLeague.getTopTeam(), awayTeam);
		} catch (TeamException | LeagueException e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests that the league returns the correct bottom team after a match
	 */
	@Test
	public void getBottomTeamTester(){
		SoccerLeague testLeague = new SoccerLeague(matchCapacity);
		try {
			SoccerTeam homeTeam = new SoccerTeam("home", "team");
			SoccerTeam awayTeam = new SoccerTeam("away", "team");
			testLeague.registerTeam(homeTeam);
			testLeague.registerTeam(awayTeam);
			testLeague.startNewSeason();
			testLeague.playMatch("home", homeGoals, "away", awayGoals);
			assertEquals(testLeague.getBottomTeam(), homeTeam);
		} catch (TeamException | LeagueException e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether teams are sorted correctly
	 */
	@Test
	public void sortTeamsTester(){
		SoccerLeague testLeague = new SoccerLeague(matchCapacity);
		try {
			SoccerTeam homeTeam = new SoccerTeam("home", "team");
			SoccerTeam awayTeam = new SoccerTeam("away", "team");
			testLeague.registerTeam(homeTeam);
			testLeague.registerTeam(awayTeam);
			testLeague.startNewSeason();
			testLeague.playMatch("home", homeGoals, "away", awayGoals);
			testLeague.sortTeams();
			assertEquals(testLeague.getTopTeam(), homeTeam);
			assertEquals(testLeague.getBottomTeam(), awayTeam);
		} catch (TeamException | LeagueException e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether a specified team can be found in the league
	 */
	@Test
	public void containsTeamTester(){
		SoccerLeague testLeague = new SoccerLeague(capacity);
		try {
			SoccerTeam homeTeam = new SoccerTeam("home", "team");
			testLeague.registerTeam(homeTeam);
			assertEquals(testLeague.containsTeam("home"), true);
		} catch (TeamException | LeagueException e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
}


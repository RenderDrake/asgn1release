package asgn1Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerCompetition;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerCompetition class
 *
 * @author Alan Woodley
 *
 */
public class SoccerCompetitionTests {
	
	private final int numLeagues = 1;
	private final int numTeams = 2;
	private final int firstLeague = 0;
	/*
	 * Tests whether a new competition can be made
	 */
	@Test
	public void SoccerCompetitionTester(){
		SoccerCompetition testComp = new SoccerCompetition("test", numLeagues, numTeams);
	}
	/*
	 * Tests whether a league can be returned from the competition
	 */
	@Test
	public void getLeagueTester(){
		SoccerCompetition testComp = new SoccerCompetition("test", numLeagues, numTeams);
		try {
			testComp.getLeague(firstLeague);
		} catch (CompetitionException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the competition's season can be started
	 */
	@Test
	public void startSeasonTester(){
		SoccerCompetition testComp = new SoccerCompetition("test", numLeagues, numTeams);
		try {
			testComp.getLeague(firstLeague).registerTeam(new SoccerTeam("test", "team"));
			testComp.getLeague(firstLeague).registerTeam(new SoccerTeam("dummy", "team"));
			testComp.startSeason();
		} catch (LeagueException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (CompetitionException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	/*
	 * Tests whether the competition's season can be ended
	 */
	@Test
	public void endSeasonTester(){
		SoccerCompetition testComp = new SoccerCompetition("test", numLeagues, numTeams);
		try {
			testComp.getLeague(firstLeague).registerTeam(new SoccerTeam("test", "team"));
			testComp.getLeague(firstLeague).registerTeam(new SoccerTeam("dummy", "team"));
			testComp.startSeason();
			testComp.endSeason();
		} catch (LeagueException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (CompetitionException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (TeamException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}


package asgn1Tests;

import static org.junit.Assert.*;


import org.junit.Test;

import asgn1SoccerCompetition.SportsTeamForm;
import asgn1SportsUtils.WLD;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerTeamForm class
 *
 * @author Alan Woodley
 *
 */
public class SportsTeamFormTests {
	
	SportsTeamForm teamFormA = new SportsTeamForm();
	SportsTeamForm teamFormB = new SportsTeamForm();
	SportsTeamForm teamFormC = new SportsTeamForm();
	SportsTeamForm teamFormD = new SportsTeamForm();
	
	private final int maxLength = 5;
	
	/**
	 * Tests the newly created SportsTeamForm for the correct default values
	 */
	@Test
	public void SportsTeamFormTester(){
		assertEquals(teamFormA.toString(), "-----");
	}
	/**
	 * Tests whether a result can be added to a form and be shown correctly
	 */
	@Test
	public void addResultToFormTester(){
		teamFormB.addResultToForm(WLD.WIN);
		assertEquals(teamFormB.toString(), "W----");
	}
	/*
	 * Tests whether a result's form string is returned correctly
	 */
	@Test
	public void toStringTester(){
		teamFormC.addResultToForm(WLD.WIN);
		teamFormC.addResultToForm(WLD.WIN);
		teamFormC.addResultToForm(WLD.WIN);
		teamFormC.addResultToForm(WLD.WIN);
		teamFormC.addResultToForm(WLD.WIN);
		teamFormC.addResultToForm(WLD.LOSS);
		assertEquals("LWWWW", teamFormC.toString());
	}
	/*
	 * Tests whether a form's number of games is returned correctly
	 */
	@Test
	public void getNumGamesTester(){
		teamFormD.addResultToForm(WLD.WIN);
		teamFormD.addResultToForm(WLD.WIN);
		teamFormD.addResultToForm(WLD.WIN);
		teamFormD.addResultToForm(WLD.WIN);
		teamFormD.addResultToForm(WLD.WIN);
		teamFormD.addResultToForm(WLD.LOSS);
		assertEquals(maxLength, teamFormD.getNumGames());
	}
	/*
	 * Tests whether a form can be reset correctly
	 */
	@Test
	public void resetFormTester(){
		SportsTeamForm teamFormE = new SportsTeamForm();
		teamFormE.addResultToForm(WLD.WIN);
		teamFormE.resetForm();
		assertEquals("-----", teamFormE.toString());
	}
		
}
